//
//  CXLoginViewController.m
//  Colfax
//
//  Created by Thomas Schmidt on 6/27/14.
//  Copyright (c) 2014 Colfax. All rights reserved.
//

#import "CXLoginViewController.h"
#import "CXConstants.h"
#import <Parse/Parse.h>

@interface CXLoginViewController ()
@property (weak, nonatomic) IBOutlet UITextField *usernameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;

@end

@implementation CXLoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)loginPressed:(id)sender {
    
    NSString *username = [self.usernameField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *password = [self.passwordField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if (username.length == 0 || password.length == 0)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops!"
                                                            message:@"Make sure you enter a username and password!"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
    else {
        [PFUser logInWithUsernameInBackground:username password:password block:^(PFUser *user, NSError *error)
         {
             if (error)
             {
                 UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Sorry" message:[error.userInfo objectForKey:@"error"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                 [alertView show];
             }
             else {
                 if([[user objectForKey:kCXUserRoleKey] isEqualToString:kCXRoleCoachKey] || [[user objectForKey:kCXUserRoleKey] isEqualToString:kCXRoleCoachKey]) {
                     [self.navigationController performSegueWithIdentifier:@"segueToConversationsView" sender:self];
                     
                 } else if([[user objectForKey:kCXUserRoleKey] isEqualToString:kCXRoleClientKey]) {
                     [self.navigationController performSegueWithIdentifier:@"clientSegueToMessagesView" sender:self];
                 }
             }
             
         }];
    }

}
- (IBAction)signupPressed:(id)sender {
}

@end
