//
//  main.m
//  Colfax
//
//  Created by Thomas Schmidt on 6/27/14.
//  Copyright (c) 2014 Colfax. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CXAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CXAppDelegate class]));
    }
}
