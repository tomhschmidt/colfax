//
//  CXConstants.h
//  Colfax
//
//  Created by Thomas Schmidt on 6/27/14.
//  Copyright (c) 2014 Colfax. All rights reserved.
//

#ifndef Colfax_CXConstants_h
#define Colfax_CXConstants_h

static NSString* const kCXUserRoleKey = @"role";

static NSString* const kCXRoleAdminKey = @"admin";
static NSString* const kCXRoleCoachKey = @"coach";
static NSString* const kCXRoleClientKey = @"client";
#endif
